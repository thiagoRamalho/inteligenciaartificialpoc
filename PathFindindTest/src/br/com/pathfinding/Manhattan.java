package br.com.pathfinding;

/**
 * @author       $h@rk
 * @Date         02/06/2012
 * @project      PathFindindTest
 * @Description: 
 */
public class Manhattan implements Heuristica {

	/* (non-Javadoc)
	 * @see br.com.pathfinding.Heuristica#calcularCustoDeslocamento(br.com.pathfinding.Coordenada, br.com.pathfinding.Coordenada)
	 */
	@Override
	public int calcularCustoDeslocamento(Coordenada origem, Coordenada destino) {
		
		int destinoX = Math.abs(origem.getX() - destino.getX());
		int destinoY = Math.abs(origem.getY() - destino.getY());
		
		return 10 * (destinoX + destinoY);
	}

}
