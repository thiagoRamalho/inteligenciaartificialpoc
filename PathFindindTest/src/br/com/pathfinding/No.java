package br.com.pathfinding;



public class No implements Coordenada{
	
	private int x;
	private int y;
	private No noPai;
	private int g;
	private int h;
	private boolean isObstaculo;
	
	public No(int x, int y,  No noPai, boolean isObstaculo){
		this.x = x;
		this.y = y;		
		this.noPai = noPai;
		this.isObstaculo = isObstaculo;
	}
	
	public No(int x, int y){
		this(x, y, null, false);
	}

	public No(int x, int y, boolean isObstaculo){
		this(x, y, null, isObstaculo);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setPai(No noPai) {
		this.noPai = noPai;
	}
	
	public No getNoPai(){
		return noPai;
	}
	
	public No clone(){
		//seta um n� para evitar nullPointer
		return new No(x, y, this.noPai, isObstaculo);
	}

	public boolean isObstaculo() {		
		return isObstaculo;
	}

	@Override
	public String toString() {		
		return "No [F=" + this.getF() + ", G=" + g  + ", H=" + h + " x=" + x + ", y=" + y + " Obstaculo "+isObstaculo+" ]";
	}

	public float getF() {		
		return g + h;
	}
	
	public void setG(int g) {

		if(this.noPai!=null){
			g = g + this.noPai.getG();
		}
		
		this.g = g;
	}
	
	public int getG() {
		return g;
	}

	public float getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		No other = (No) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}


	@Override
	public int compareTo(No o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getCusto() {
		// TODO Auto-generated method stub
		return 0;
	}
}
