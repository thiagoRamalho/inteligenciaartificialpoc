package br.com.pathfinding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author       $h@rk
 * @Date         25/05/2012
 * @project      PathFindindTest
 * @Description: 
 */
public class AStar {

	private List<No> listaAberta;
	private List<No> listaFechada;
	//profundidade m�xima de pesquisa
	private final int PROFUNDIDADE = 2;


	public AStar(){
		this.listaAberta  = new ArrayList<No>();
		this.listaFechada = new ArrayList<No>();
	}
	
	
	
	public int custoEuclidiano(No origem, No noDestino){
		return (int) Math.sqrt (Math.pow ((origem.getX() - noDestino.getX()), 2) + Math.pow ((origem.getY() - noDestino.getY()), 2));
	}
	
	/**
	 * Pesquisa o melhor caminho para percorrer dentro do mapa especificado
	 * 
	 * @param mapa      - Terreno a ser percorrido
	 * @param noInicio  - Ponto de origem
	 * @param noDestino - Ponto de destino
	 * @return
	 */

	/**
	 * Pesquisa o melhor caminho para percorrer dentro do mapa especificado
	 * 
	 * @param mapa      - Terreno a ser percorrido
	 * @param noInicio  - Ponto de origem
	 * @param noDestino - Ponto de destino
	 * @return
	 */
	public Collection<No> pesquisarCaminho(final List<No> mapa, final No noInicio, final No noDestino){

		//limpa as listas para evitar vest�gios
		this.listaAberta.clear();
		this.listaFechada.clear();

		//clonamos o n� inicial para evitar erros de manipula��o
		No noCorrente = noInicio.clone();
		noCorrente.setG(0);		
		noCorrente.setH(calcularDeslocamentoVizinhoDestino(noCorrente, noDestino));

		//adiciona o n� corrente a lista de possibilidades
		this.listaAberta.add(noCorrente);

		do {	

			//carrega todos os vizinhos em volta do n� corrente
			Collection<No> listaVizinhos = this.encontrarVizinhos(noCorrente);

			for (No noVizinho : listaVizinhos) {

				if(!this.isObstaculo(noVizinho, mapa) && !this.listaFechada.contains(noVizinho)){

					if(!this.listaAberta.contains(noVizinho)){

						//adiciona o n� corrente como pai dos vizinhos
						noVizinho.setPai(noCorrente);

						//calcula o deslocamento do vizinho at� o corrente 
						float g = this.calcularDeslocamentoVizinhoDestino(noVizinho, noCorrente);

						//se n�o est� na lista de possibilidades
						//calcula o custo de deslocamento do vizinho at� o no corrente (G)
						noVizinho.setG(g + noVizinho.getG());
						
						float h = this.calcularDeslocamentoVizinhoDestino(noVizinho, noDestino);
						
						noVizinho.setH(h);						

						//guarda todos os vizinhos a lista de possibilidades
						this.listaAberta.add(noVizinho);
						
					} else {

						int indexNo = this.listaAberta.lastIndexOf(noVizinho);

						noVizinho = this.listaAberta.get(indexNo);

						No noVizinhoClone = noVizinho.clone();
						
						//calcula o deslocamento do vizinho at� o corrente 
						float g = this.calcularDeslocamentoVizinhoDestino(noVizinho, noCorrente);

						//se n�o est� na lista de possibilidades
						//calcula o custo de deslocamento do vizinho at� o no corrente (G)
						noVizinhoClone.setG(g + noVizinho.getG());
						
						//se j� existe na lista aberta, ent�o verifique o seu custo G
						if(noVizinhoClone.getG() < noVizinho.getG()){

							noVizinho.setPai(noCorrente);

							//calcula o deslocamento do vizinho at� o corrente 
							float g1 = this.calcularDeslocamentoVizinhoDestino(noVizinho, noCorrente);

							//se n�o est� na lista de possibilidades
							//calcula o custo de deslocamento do vizinho at� o no corrente (G)
							noVizinho.setG(g1 + noCorrente.getG());
						}
					}
				}
			}

			//Remover o n� corrente da lista de abertos
			this.listaAberta.remove(noCorrente);

			//Adicionar o n� atual na lista de fechados
			this.listaFechada.add(noCorrente.clone());

			Collections.sort(this.listaAberta);

			//Escolher da lista de abertos o no de menor custo
			noCorrente = this.listaAberta.get(0);

		} while (!noCorrente.equals(noDestino));


		return this.criarRota();
	}
	  
	
	
	private boolean isObstaculo(No no, List<No> mapa) {

		boolean isObstaculo = true;

		if(no.getX() > -1 || no.getY() > -1){
			int indiceNo = mapa.lastIndexOf(no);

			if(indiceNo < 0){
				return true;
			}
			
			No noPosicao = mapa.get(indiceNo);

			isObstaculo = noPosicao.isObstaculo();
		}

		return isObstaculo;
	}

	/**
	 * Calcula os passos necess�rios para deslocar do vizinho 
	 * at� o destino 
	 * 
	 * @param noVizinho
	 * @param noDestino
	 */
	private float calcularDeslocamentoVizinhoDestino(No noVizinho, No noDestino) {
		return Math.abs(noVizinho.getX() - noDestino.getX() ) +  Math.abs(noVizinho.getY() - noDestino.getY());		
	}

	private int calcularDeslocamentoOrigemVizinho(No noVizinho, No noOrigem) {

		int g = 14;

		//se for linha reta seta 10, sen�o mant�m o valor 14 (diagonal)
		if (noVizinho.getX() == noOrigem.getX() || noVizinho.getY() == noOrigem.getY())
			g = 10;
		
		return g;
	}

	public Collection<No> encontrarVizinhos(No noInicial) {

		Collection<No> nosVizinhos = new ArrayList<No>();

		int indiceX = noInicial.getX();

		//recua um �ndice para a linha
		indiceX--;

		//anda pelas linhas
		for (int indiceLinha = indiceX; indiceLinha < (noInicial.getX()+this.PROFUNDIDADE); indiceLinha++) {

			int indiceY = noInicial.getY();

			//recua um �ndice para a coluna
			indiceY--;

			//anda pelas colunas
			for (int indiceColuna = indiceY; indiceColuna < (noInicial.getY()+this.PROFUNDIDADE); indiceColuna++) {

				//ignora �ndices negativos
				if((indiceLinha < 0 || indiceColuna < 0)){ continue;}

				//ignora se o n� contiver o mesmo �ndice do n� inicial
				if(indiceLinha == noInicial.getX() && indiceColuna == noInicial.getY()){continue;}

				No noVizinho = new No(indiceLinha, indiceColuna, null, false);

				//adiciona o novo n�
				nosVizinhos.add(noVizinho);
			}
		}

		return nosVizinhos;
	}

	public Collection<No> criarRota(){

		Collections.sort(this.listaFechada);
		
		No no = this.listaFechada.get(this.listaFechada.size()-1);

		Collection<No> rota = new ArrayList<No>();
		
		while(no.getNoPai()!= null){

			rota.add(no.clone());

			no = no.getNoPai();
		}

		return rota;
	}
}
