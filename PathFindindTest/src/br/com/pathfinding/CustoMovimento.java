package br.com.pathfinding;

/**
 * @author       $h@rk
 * @Date         02/06/2012
 * @project      PathFindindTest
 * @Description: 
 */
public interface CustoMovimento {
	
	public int calcular(No origem, No destino);
}
