package br.com.pathfinding;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

public class PathFindingImp {

	private TreeSet<No> listaFechada;
	private TreeSet<No> listaAberta;

	//profundidade m�xima de pesquisa
	private final int PROFUNDIDADE = 2;


	public PathFindingImp() {
	}

	public Collection<No> procurarCaminho(No inicio, No alvo, No limite){

		this.listaAberta  = new TreeSet<No>();
		this.listaFechada = new TreeSet<No>();

		No corrente = inicio.clone();
		//1) adicione o quadrado inicial � lista aberta.		
		listaAberta.add(corrente);

		//2) Repita o seguinte:
		do {

			//a. Procure o quadrado que tenha o menor custo de F na lista aberta. 
			//assumimos que o primeiro elemento � o melhor custo
			corrente = this.listaAberta.first();
			
			//b. Mova-o para a lista fechada.
			this.listaFechada.add(corrente);

			//c. pesquisa os n�s ajacentes do no atual
			Collection<No> nosAdjacentes = this.pesquisarAdjacentes(corrente, limite);

			//d. Para cada um dos n�s adjacentes ao n� corrente.
			for (No no : nosAdjacentes) {

				//i. Se n�o � pass�vel ou se estiver na lista fechada, ignore. 
				//Caso contr�rio fa�a o seguinte:
				if(!no.isObstaculo() && !this.listaFechada.contains(no)){
										
					//1. Se n�o estiver na lista aberta. 
					if(!this.listaAberta.contains(no)){
						//Fa�a o no corrente o pai deste no. 
						no.setPai(corrente);
						
						//Grave os custos F, G, e H ao no.
						this.calculoManhattan(no, corrente);
						
						this.calculoCusto(no, alvo);
						
						//acrescente a lista aberta
						this.listaAberta.add(no);
					}
					else {
						
						//2. Se j� estiver na lista aberta, confere para ver se este caminho para aquele quadrado � melhor,
						//usando custo G como medida.
						if(no.getG() < corrente.getG()){
							corrente.setPai(no);							
						}
					}      
				}
			}

		} while (!corrente.equals(alvo));
		
		return this.listaFechada.descendingSet();		
	}


	private void calculoCusto(No inicial, No alvo) {
		inicial.setH(10 * (Math.abs(inicial.getX() - alvo.getX()) +  Math.abs(inicial.getY() - alvo.getY())));		
	}

	/**
	 * Dado o n� atual pesquisa pelos seus n�s
	 * adjacentes
	 * @return
	 */
	public List<No> pesquisarAdjacentes(No noInicial, No limite){

		List<No> nosAdjacentes = new ArrayList<No>();

		int indiceX = noInicial.getX();

		//recua um �ndice para a linha
		indiceX--;

		//anda pelas linhas
		for (int indiceLinha = indiceX; indiceLinha < (noInicial.getX()+this.PROFUNDIDADE); indiceLinha++) {

			int indiceY = noInicial.getY();

			//recua um �ndice para a coluna
			indiceY--;

			//anda pelas colunas
			for (int indiceColuna = indiceY; indiceColuna < (noInicial.getY()+this.PROFUNDIDADE); indiceColuna++) {

				//ignora �ndices negativos
				if((indiceLinha < 0 || indiceColuna < 0)){ continue;}

				//ignora se o n� contiver o mesmo �ndice do n� inicial
				if(indiceLinha == noInicial.getX() && indiceColuna == noInicial.getY()){continue;}

				//se chegou ao limite do �rea, n�o realiza processo
				if(indiceLinha >= limite.getX() || indiceColuna >= limite.getY()){continue;}

				No noAjacente = new No(indiceLinha, indiceColuna);
				
				//adiciona o novo n�
				nosAdjacentes.add(noAjacente);
			}
		}

		return nosAdjacentes;
	}
	
	public No calculoManhattan(No inicial, No alvo){
		
		float g = 14;
		
		//se for linha reta seta 10, sen�o mant�m o valor 14 (diagonal)
		if (inicial.getX() == alvo.getX() || inicial.getY() == alvo.getY())
			g = 10;
		
		//acumula o valor do pai ao resultado
		g = (inicial.getNoPai() != null ? inicial.getNoPai().getG() : 0) + g;
		
		inicial.setG(g);
		
		return inicial;
	}
}
