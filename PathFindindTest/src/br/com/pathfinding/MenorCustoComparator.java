package br.com.pathfinding;

import java.util.Comparator;

public class MenorCustoComparator implements Comparator<No> {

	@Override
	public int compare(No o1, No o2) {
		
		int resultado = 0;
		
		if(o1.getF() > o2.getF()){
			resultado = 1;
		}
		
		else if(o1.getF() < o2.getF()){
			resultado = -1;
		}		
		
		return resultado;
	}
	
	/**
	 * Usado para crit�rio de desempate
	 * @return
	 */
	private int comparePorH(No o1, No o2){
		
		int resultado = 0;
		
		if(o1.getH() > o2.getH()){
			resultado = 1;
		}
		
		else if(o1.getH() < o2.getH()){
			resultado = -1;
		}		
		
		return resultado;
	}

}
