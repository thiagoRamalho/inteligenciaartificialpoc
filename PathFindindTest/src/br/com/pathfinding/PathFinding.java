package br.com.pathfinding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author       $h@rk
 * @Date         30/05/2012
 * @project      PathFindindTest
 * @Description: 
 */
public final class PathFinding {
	
	
	private Heuristica heuristica;
	private List<No> listaFechada;
	private List<No> listaAberta;
	private CustoMovimento custoMovimento;

	public PathFinding(){
		this(new Manhattan(), new CustoOrtogonal());
	}
	
	public PathFinding(Heuristica heuristica, CustoMovimento custoMovimento) {
		super();
		this.heuristica   = heuristica;
		this.listaAberta  = new ArrayList<No>();
		this.listaFechada = new ArrayList<No>();
		this.custoMovimento = custoMovimento;
	}
	
	/**
	 * Descobre o melhor caminho do noInicio at� o noFim desviando de obst�culos
	 * existentes no Terreno
	 * 
	 * @param noInicio
	 * @param noDestino
	 * @param terreno
	 * @return
	 */
	public Collection<No> descobrirCaminho(final No noInicio, final No noDestino, final List<No> terreno){
		
		this.listaAberta.clear();
		this.listaFechada.clear();
		
		No noCorrente = noInicio.clone();
		
		//1 - adicione o no inicial a lista aberta
		this.listaAberta.add(noCorrente);
		
		do {			
						
			//2 - Procure o n� que tenha o menor custo de F na lista aberta
			noCorrente = this.listaAberta.get(0);

			this.listaAberta.remove(noCorrente);
			
			//a - Mova-o para a lista fechada
			this.listaFechada.add(noCorrente);
			
			//carrega os n�s adjacentes
			Collection<No> listaAdjacentes = this.pesquisarAdjacentes(noCorrente);
			
			//b - Para cada um dos 8 n�s adjacente ao n� corrente...
			for (No noAdjacente : listaAdjacentes) {

				//i. Se n�o � pass�vel ou se estiver na lista fechada, ignore
				if(!this.isObstaculo(noAdjacente, terreno) && !this.listaFechada.contains(noAdjacente)){

					//Se n�o estiver na lista aberta... 
					if(!this.listaAberta.contains(noAdjacente)){

						//Fa�a o n� corrente o pai do n� adjacente
						noAdjacente.setPai(noCorrente);

						//calcula o custo de movimento entre o n� adjacente e o corrente
						int g = this.custoMovimento.calcular(noAdjacente, noCorrente);						
						
						//calcula o custo do n� filho at� o destino
						int h = this.heuristica.calcularCustoDeslocamento(noAdjacente, noDestino);

						noAdjacente.setH(h);
						noAdjacente.setG(g);

						//acrescente-o � lista aberta					
						this.listaAberta.add(noAdjacente);					
					}
					
					//Se j� estiver na lista aberta...
					else {

						int index   = this.listaAberta.indexOf(noAdjacente);					
						noAdjacente = this.listaAberta.get(index);
												
						//deslocamento do n� filho at� o seu pai
						int g = this.custoMovimento.calcular(noAdjacente, noCorrente);

						//confere para ver se este caminho para o n� corrente � melhor, 
						//usando custo G como medida. Um valor G mais baixo mostra que este � um caminho melhor.
						if(noAdjacente.getG() > (noCorrente.getG()+g)){

							//Nesse caso, mude o pai do n� adjacente para o n� corrente
							noAdjacente.setPai(noCorrente);
							
							noAdjacente.setG(g);
						}
					}
				}
			}
			
			Collections.sort(this.listaAberta, new MenorCustoComparator());
			
		} while (!this.listaAberta.isEmpty() || !this.listaFechada.contains(noDestino));
		
		return this.rota();
	}


	public boolean isObstaculo(No no, List<No> terreno) {

		boolean isObstaculo = true;

		if(no.getX() > -1 || no.getY() > -1){
			int indiceNo = terreno.lastIndexOf(no);

			if(indiceNo < 0){
				return true;
			}
			
			No noPosicao = terreno.get(indiceNo);

			isObstaculo = noPosicao.isObstaculo();
		}

		return isObstaculo;
	}

	public List<No> pesquisarAdjacentes(No no) {
		
		List<No> lista = new ArrayList<No>();
		
		int limiteX = no.getX();
		int limiteY = no.getY();
		
		for (int indiceX = (limiteX -1); indiceX < (limiteX+2); indiceX++) {
			
			for (int indiceY = (limiteY-1); indiceY < (limiteY+2); indiceY++) {
				
				No criado = new No(indiceX, indiceY);
				
				if(!criado.equals(no)){
					lista.add(criado);
				}				
			}
		}
		
		return lista;
	}

	private Collection<No> rota() {
		
		No no = this.listaFechada.get(this.listaFechada.size()-1);

		Collection<No> rota = new ArrayList<No>();
		
		while (no != null) {			
			
			rota.add(no);

			no = no.getNoPai();
		}

		return rota;
	}
	
}
