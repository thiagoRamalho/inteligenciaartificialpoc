package br.com.pathfinding;

/**
 * @author       $h@rk
 * @Date         30/05/2012
 * @project      PathFindindTest
 * @Description: 
 */
public interface Heuristica {

	/**
	 * Calcula o custo de movimento do n� origem at� o n� destino
	 * 
	 * @param origem
	 * @param destino
	 * @return
	 */
	public int calcularCustoDeslocamento(Coordenada origem, Coordenada destino);
}
