package br.com.pathfinding;

/**
 * @author       $h@rk
 * @Date         02/06/2012
 * @project      PathFindindTest
 * @Description: 
 */
public class CustoDiagonal implements CustoMovimento {

	/* (non-Javadoc)
	 * @see br.com.pathfinding.CustoG#calcularCusto(br.com.pathfinding.No, br.com.pathfinding.No)
	 */
	@Override
	public int calcular(No origem, No destino) {
		
		int g = 14;
		
		//se for linha reta seta 10, sen�o mant�m o valor 14 (diagonal)
		if (origem.getX() == destino.getX() || origem.getY() == destino.getY())
			g = 10;

		
		return g;
	}

}
