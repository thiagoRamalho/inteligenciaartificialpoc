package br.com.pathfinding;


/**
 * @author       $h@rk
 * @Date         30/05/2012
 * @project      PathFindindTest
 * @Description: Representa uma coordenada no espa�o pelos pontos x e y
 */
public interface Coordenada extends Comparable<No>{

	public int getX();
	
	public int getY();
	
	public boolean equals(Object obj);
	
	public float getCusto();
}
