package br.com.pathfinding;

public class Nodo implements Coordenada {

	private int x;
	private int y;
	private boolean isObstaculo;
	private float g;
	private float h;
	
	public Nodo(int x, int y, boolean isObstaculo) {
		super();
		this.x = x;
		this.y = y;
		this.isObstaculo = isObstaculo;
	}


	@Override
	public int compare(Coordenada outra) {
		
		if(this.getCusto() < outra.getCusto()){
			return -1;
		}
		
		if(this.getCusto() > outra.getCusto()){
			return 1;
		}
		
		return 0;
	}


	@Override
	public boolean equals(Coordenada outra) {
		
		if(this.getX() != outra.getX()){
			return false;
		}
		
		if(this.getY() != outra.getY()){
			return false;
		}
		
		return true;
	}

	public float getCusto(){
		return g + h;
	}

	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	public boolean isObstaculo() {
		return isObstaculo;
	}

	
}
