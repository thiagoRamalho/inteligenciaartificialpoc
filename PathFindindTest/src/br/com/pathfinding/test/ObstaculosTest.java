package br.com.pathfinding.test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import br.com.pathfinding.No;
import br.com.pathfinding.PathFinding;

public class ObstaculosTest {

	private ArrayList<No> terreno;

	@Before
	public void setUp() throws Exception {
		//cria lista de obst�culos
		terreno = new ArrayList<No>();
		terreno.add(new No(1, 3, true));
		terreno.add(new No(2, 3, true));
		terreno.add(new No(3, 3, true));
	}

	@Test
	public void deveConsiderarTodosComoObstaculos(){
		
		PathFinding p = new PathFinding();
		
		assertTrue(p.isObstaculo(new No(1, 3), terreno));
		assertTrue(p.isObstaculo(new No(2, 3), terreno));
		assertTrue(p.isObstaculo(new No(3, 3), terreno));
	}
	
	@Test
	public void apenasUmNoNaoEObstaculo(){
		
		PathFinding p = new PathFinding();
		
		terreno.add(new No(4, 3, false));
		
		assertTrue(p.isObstaculo(new No(1, 3), terreno));
		assertTrue(p.isObstaculo(new No(2, 3), terreno));
		assertTrue(p.isObstaculo(new No(3, 3), terreno));		
		assertFalse(p.isObstaculo(new No(4, 3), terreno));
	}
	
	
	@Test
	public void deveConsiderarObstaculoNosInexistentesNoTerreno(){
		
		PathFinding p = new PathFinding();
		
		terreno.add(new No(4, 3, false));
		
		assertTrue(p.isObstaculo(new No(1, 3), terreno));
		assertTrue(p.isObstaculo(new No(2, 3), terreno));
		assertTrue(p.isObstaculo(new No(3, 3), terreno));
		
		assertFalse(p.isObstaculo(new No(4, 3), terreno));
		
		//n�s inexistentes na lista do terreno
		assertTrue(p.isObstaculo(new No(5, 5), terreno));
		assertTrue(p.isObstaculo(new No(4, 5), terreno));
		assertTrue(p.isObstaculo(new No(9, 0), terreno));
	}
	
	@Test
	public void deveConsiderarObstaculoIndicesNegativos(){
		
		PathFinding p = new PathFinding();
		
		terreno.add(new No(4, 3, false));
		
		//obst�culo pois foi inicializado como tal
		assertTrue(p.isObstaculo(new No(1, 3), terreno));
		assertTrue(p.isObstaculo(new No(2, 3), terreno));

		//�ndices negativos (obst�culo)
		assertTrue(p.isObstaculo(new No(-4, 5),   terreno));
		assertTrue(p.isObstaculo(new No(9, -999), terreno));

		assertTrue(p.isObstaculo(new No(3, 3), terreno));
		
		//n�o � obst�culo
		assertFalse(p.isObstaculo(new No(4, 3), terreno));
		
		//� obst�culo pois n�o existe no terreno
		assertTrue(p.isObstaculo(new No(5, 5), terreno));
		
	}
}
