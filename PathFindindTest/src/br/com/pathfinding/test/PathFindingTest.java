package br.com.pathfinding.test;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.pathfinding.CustoDiagonal;
import br.com.pathfinding.Manhattan;
import br.com.pathfinding.No;
import br.com.pathfinding.PathFinding;

public class PathFindingTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void deveEncontrarMelhorCaminhoComTresBlocosObstaculos(){
		
		//n� atual (no diagrama letra I)
		No noInicio  = new No(2, 1, null, false);
		No noDestino = new No(2, 5, null, false);
			
		//dado que a posi��o inicial 
		Collection<No> resultado = new PathFinding(new Manhattan(), new CustoDiagonal()).descobrirCaminho(noInicio, noDestino, criarMapa());
		
		for (No no : resultado) {
			System.out.println(no);
		}
		
		//conforme o diagrama, devem ser encontrados 8 n�s
		assertEquals(6, resultado.size());
	}
	
	public List<No> criarMapa(){
		
		//cria lista de obst�culos
		List<No> obstaculo = new ArrayList<No>();
		obstaculo.add(new No(1, 3));
		obstaculo.add(new No(2, 3));
		obstaculo.add(new No(3, 3));
		
		return this.getMapa(6, 7, obstaculo);
	}

	
	private List<No> getMapa(int linhas, int colunas, List<No> obstaculo){
		 
		List<No> lista = new ArrayList<No>();
		
		for(int i = 0; i < linhas; i++){

			for(int j = 0; j < colunas; j++){
											
				No pontoMapa = new No(i, j, this.isObstaculo(obstaculo, i, j));
				
				lista.add(pontoMapa);
			}
		}
		
		return lista;
	}
	
	private boolean isObstaculo(List<No> listaObstaculo, int i, int j){
		boolean isObstaculo = false;
		for (No no : listaObstaculo) {
			if(no.getX() == i && no.getY() == j){
				isObstaculo = true;
				break;
			}
		}
		
		return isObstaculo;
	}
}
