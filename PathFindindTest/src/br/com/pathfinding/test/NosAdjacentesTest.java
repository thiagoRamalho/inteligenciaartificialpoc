package br.com.pathfinding.test;


import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.pathfinding.No;
import br.com.pathfinding.PathFinding;

public class NosAdjacentesTest {

	private static int count;

	@Before
	public void setUp() throws Exception {
		System.out.println("----- "+(++count)+"-----");
	}

	
	@Test
	public void deveEncontrarNosAdjacentesPegaTodasDirecoes(){
		
		//n� atual (no diagrama letra I)
		No noAtual = new No(1, 1);
		
		//dado que a posi��o inicial 
		List<No> resultado = new PathFinding(null).pesquisarAdjacentes(noAtual);
		
		//devem ser encontrados 8 n�s
		assertEquals(8, resultado.size());
		
		assertIndices(resultado.get(0), 0,0);
		assertIndices(resultado.get(1), 0,1);
		assertIndices(resultado.get(2), 0,2);
		assertIndices(resultado.get(3), 1,0);
		assertIndices(resultado.get(4), 1,2);
		assertIndices(resultado.get(5), 2,0);
		assertIndices(resultado.get(6), 2,1);
		assertIndices(resultado.get(7), 2,2);
	}

	private void assertIndices(No no, int valorX, int valorY) {

		System.out.println(no.toString());
		assertEquals(no.getX(), valorX);
		assertEquals(no.getY(), valorY);

	}
}
