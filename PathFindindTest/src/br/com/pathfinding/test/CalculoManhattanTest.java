package br.com.pathfinding.test;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.pathfinding.Heuristica;
import br.com.pathfinding.Manhattan;
import br.com.pathfinding.No;

public class CalculoManhattanTest {

	
	@Test
	public void calculoHorizontalDireto(){
		
		No noAtual = new No(2, 1);
		No destino = new No(2, 5);
		
		Heuristica p = new Manhattan();
		
    	int resultado = p.calcularCustoDeslocamento(noAtual, destino);
		
		assertEquals(40, resultado);
	}

	@Test
	public void calculoComCurva(){
		
		No noAtual = new No(0, 0);
		No destino = new No(2, 5);
		
		Heuristica p = new Manhattan();
		
    	int resultado = p.calcularCustoDeslocamento(noAtual, destino);
		
		assertEquals(70, resultado);
	}

	@Test
	public void calculoComCurvaPontosEmDiagonal(){
		
		No noAtual = new No(5, 0);
		No destino = new No(0, 6);
		
		Heuristica p = new Manhattan();
		
    	int resultado = p.calcularCustoDeslocamento(noAtual, destino);
		
		assertEquals(110, resultado);
	}

}
