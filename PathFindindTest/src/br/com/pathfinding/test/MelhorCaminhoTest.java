package br.com.pathfinding.test;


import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import br.com.pathfinding.No;
import br.com.pathfinding.PathFinding;

public class MelhorCaminhoTest {

	private static int count;


	@Before
	public void setUp() throws Exception {
		System.out.println("----- "+(++count)+"-----");
	}

	
	/**
	 * Mapeamento de �rea 3x5
	 * 
	 * Letra A n� adjacente
	 * Letra I n� inicial
	 * Letra B n� bloqueado
	 * Letra F n� final
	 * 
	 * X0123456
	 * 0CCCCCCC
	 * 1CCCBCCC
	 * 2CICBCFC
	 * 3CCCBCCC
	 * 4CCCCCCC
	 */
	@Test
	public void deveEncontrarMelhorCaminhoComTresBlocosObstaculos(){
		
		//n� atual (no diagrama letra I)
		No noAtual = new No(2, 1, null);
		
		//dado que a posi��o inicial 
		Collection<No> resultado = new PathFinding().procurarCaminho(noAtual, new No(2, 6, null), new No(6,7, null));
		
		//conforme o diagrama, devem ser encontrados 8 n�s
		assertEquals(6, resultado.size());
	}

}
