package br.com.pathfinding;

/**
 * @author       $h@rk
 * @Date         02/06/2012
 * @project      PathFindindTest
 * @Description: 
 */
public class CustoOrtogonal implements CustoMovimento {

	/* (non-Javadoc)
	 * @see br.com.pathfinding.CustoG#calcularCusto(br.com.pathfinding.No, br.com.pathfinding.No)
	 */
	@Override
	public int calcular(No origem, No destino) {
		//ignora o tipo de movimento
		return 10;
	}

}
